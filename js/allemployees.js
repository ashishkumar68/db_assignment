/*** allemployees.js for Handling events on allemployees.php Page.
 *
 * JS version 1.5
 * @category JS
 * @package js
 * @subpackage None
 * @author Ashish Kumar
 * @license Project Name None
 * @link None
 */
$(document).ready(function () {

    /**
     * To add error message to error div..
     * @access public
     * @param  Array $errors
     * @return None
     */
    function addErrorsToErrorList ($errors) {
        $.each($errors, function (key,error) {
            $("#errors").children("div").append("<br>"+error+"<br>");
        });
    }

    /**
     * To parse,validate and save Employees Data in file.
     * @access public
     * @param  None
     * @return None
     */
    function parseValidateAndUploadFile () {
        $.ajax({
            "type": "POST",
            "url": "parseValidateSaveEmp.php",
            "success": function (response) {
                console.log(response);
                var errorlist = JSON.parse(response);
                if (!errorlist.status) {
                    $("#errors").show();
                    $("#errors").children("div").html("");
                    if (errorlist['columnErrors']) {
                        addErrorsToErrorList(errorlist['columnErrors']);
                    }
                    if (errorlist['errors']) {
                        addErrorsToErrorList(errorlist['errors']);
                    }
                }
                else {
                    if ($("#errors")) {
                        $("#errors").hide();
                    }
                    Materialize.toast('File Uploaded Successfully', 4000);
                    $("#fileUploadForm").trigger("reset");
                }
            }
        });
    }

    /**
     * To upload csv file.
     * @access public
     * @param  None
     * @return None
     */
    function uploadFile () {
        var file = $("#empDataFile")[0].files[0];
        if (file.type.split("/")[1] !== "csv") {
            Materialize.toast('ERROR! Selected File is not CSV.', 4000);
        }
        else {
            var formdata = new FormData();
            formdata.append("empDataFile", file);
            $(".progress").show();
            var ajax = new XMLHttpRequest();
            ajax.upload.addEventListener("progress", progressHandler, false);
            ajax.addEventListener("load", completeHandler, false);
            ajax.addEventListener("error", errorHandler, false);
            ajax.addEventListener("abort", abortHandler, false);
            ajax.open("POST", "fileUpload.php");
            ajax.send(formdata);
        }
    }

    /**
     * To upload csv file.
     * @access public
     * @param  None
     * @return None
     */
    function progressHandler (event) {
        var percent = (event.loaded / event.total) * 100;
        $(".progress .determinate").css("width :Math.round(percent)");
        $("#msg").html(Math.round(percent)+"% uploaded... please wait");
    }

    /**
     * To execute while complete upload csv file.
     * @access public
     * @param  None
     * @return None
     */
    function completeHandler (event) {
        parseValidateAndUploadFile();
        $("#msg").html("Upload Complete");
        $(".progress").hide();
        $(".progress").parent().next().hide();
    }

    /**
     * To execute while some error in Upload csv file.
     * @access public
     * @param  None
     * @return None
     */
    function errorHandler (event) {
        $("#msg").html("Some Error.");
        $(".progress").hide();
        $(".progress").parent().next().hide("slow");
    }

    /**
     * To execute while upload csv file aborted.
     * @access public
     * @param  None
     * @return None
     */
    function abortHandler (event) {
        $("#msg").html("Upload Aborted.");
        $(".progress").hide();
        $(".progress").parent().next().hide("slow");
    }

    // To hide the progress bar on Page Ready..
    $(".progress").hide();

    // To Hide Errors div on Page Ready..
    $("#errors").hide();

    // To run when Form is submitted.
    $("#fileUploadForm").on("submit", function (event) {
        event.preventDefault();
        uploadFile();
    });

});