<?php
    /*** parseValidateSaveEmp.php for parsing,Validating and Saving Data into DB.
     *
     * PHP version 5
     * @category None
     * @package None
     * @subpackage None
     * @author Ashish Kumar
     * @license Project Name None
     * @link None
     */
    require_once('libraries/Database.php');
    require_once('config/constants.php');
    require_once('helpers/UtilityHelper.php');
    $empDataFile = "";
    if (!is_dir(EMPDATADIR)) {
        mkdir(EMPDATADIR,"0777");
    }
    $dbObject = new Database();
    $empDataFile .= EMPDATADIR."/".scandir(EMPDATADIR,1)[0];
    $tableRecords = convertFileDataIntoEmployeeArray(fetchAllRecordsFromFile($empDataFile),$dbObject);
    $attributes = $tableRecords['attributes'];
    $records = $tableRecords['empArray'];
    $errorlist  = validateFileRecords($attributes,$records,$dbObject);
    if (!$errorlist['status'] && sizeof($errorlist['columnErrors']) == 0) {
        $errors = $errorlist['errors'];
        foreach ($errors as $key => $error) {
            unset($tableRecords['empArray'][$key - 1]);
        }
        $employeeObjects = getEmployeeArrayFromRecords($tableRecords,$dbObject);
        if ($employeeObjects['valid']) {
            $dbObject->insertEmployeesIntoDB($employeeObjects);
        }
    }
    else if ($errorlist['status']) {
        $employeeObjects = getEmployeeArrayFromRecords($tableRecords,$dbObject);
        if ($employeeObjects['valid']) {
            $dbObject->insertEmployeesIntoDB($employeeObjects);
        }
    }
    $dbObject->closeConnection();
    echo json_encode($errorlist);
?>