<?php
    /*** fileUpload.php for File Upload in Project
     *
     * PHP version 5
     * @category None
     * @package None
     * @subpackage None
     * @author Ashish Kumar
     * @license Project Name None
     * @link None
     */
    require_once('config/constants.php');
    require_once('helpers/UtilityHelper.php');
    $fileName = $_FILES["empDataFile"]["name"]; // The file name
    $fileTmpLoc = $_FILES["empDataFile"]["tmp_name"]; // File in the PHP tmp folder
    $fileType = $_FILES["empDataFile"]["type"]; // The type of file it is
    $fileSize = $_FILES["empDataFile"]["size"]; // File size in bytes
    $fileErrorMsg = $_FILES["empDataFile"]["error"]; // 0 for false... and 1 for true
    if (!$fileTmpLoc) { // if file not chosen
        writeMessageToLogFile("ERROR: Please browse for a file before clicking the upload button.");
        exit();
    }
    if (!is_dir(EMPDATADIR)) {
        exec("mkdir ".EMPDATADIR);
        exec("chmod 777 ".EMPDATADIR);
        exec("chmod 777 ".EMPDATADIR."/");
        exec("chmod 777 ".EMPDATADIR."/*");
    }
    exec('rm -rf '.EMPDATADIR."/*");
    if (move_uploaded_file($fileTmpLoc, EMPDATADIR."/".$fileName)) {
        writeMessageToLogFile("$fileName upload is complete");
        exec("chmod 777 ".EMPDATADIR."/".$fileName);
    }
    else {
        writeMessageToLogFile("move_uploaded_file function failed");
    }
?>