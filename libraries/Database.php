<?php

	require_once('config/constants.php');
    require_once('helpers/UtilityHelper.php');

	/** * Database Class
	* Handles anything related to communication with DB.
	* @package    libraries
	* @subpackage None
	* @category   None
	* @author     Ashish
	* @link       None
	*/
	class Database {

		private $_connection;
		private $_insertSql;
		private $_updateSql = '';
		private $_deleteSql = '';
		private $_selectSql = '';

		/**
		* To initialize a new Database Object*
		* @access public
		* @param  None
		* @return None
		*/
		public function __construct () {

            $this->_connection = @new mysqli(HOST,USERNAME, PASSWORD, DB);
            // To check connection establishment
			if ($this->_connection->connect_error) {

				$message = "Error: Unable to connect to MySQL.  :".$this->_connection->connect_error;
				writeMessageToLogFile($message);
                exit;
			}
		}

		/**
		* To retrieve error in executing any Database Query.*
		* @access public
		* @param  None
		* @return string
		*/
		public function getErrorInStatementExecution () {
			return $this->_connection->error;
		}

		/**
		* To create SQL INSERT Query for an Array of Data.*
		* @access public
		* @param  string $tableName Array $data
		* @return string
		*/
		public function getInsertSqlForData ($tableName,$data) {
			$this->_insertSql = FALSE;

			// To check whether the data coming is array or not.
			if (is_array($data)) {
				$this->_insertSql = 'INSERT INTO '.$tableName.'('.implode(",", array_keys($data)).') VALUES ("'
					. implode('","', array_values($data)).'")';
			}
			return $this->_insertSql;
		}

		
		/**
		* To retrieve the result of a dynamic SQL SELECT statement using Array data,Conditions and Table Name*
		* @access public
		* @param  Array $attrs Array $conditionalData string $tableName
		* @return Array
		*/
		public function getRecordsAttributesOnCondition ($attrs,$conditionalData,$tableName) {
			if (sizeof($attrs) >= 2) {
				$this->_selectSql = "SELECT ".implode(",", $attrs)." FROM ".$tableName;
			}
			else if (sizeof($attrs) == 1) {
				$this->_selectSql = "SELECT ".$attrs[0]." FROM ".$tableName;
			}
			$whereClauses = [];
			foreach ($conditionalData as $key => $value) {
				$clause= $key." = '".$value."'";
				array_push($whereClauses, $clause);
			}
			if (sizeof($whereClauses)==1) {
				$this->_selectSql .= ' WHERE '.$whereClauses[0];
			}
			else {
				$this->_selectSql .= ' WHERE '.implode(' AND ', $whereClauses);
			}
			return $this->getResultFromSelectSql($this->_selectSql);
		}

		/**
		* To return SQL SELECT Query to retrieve all rows and columns from a Table*
		* @access public
		* @param  $table
		* @return string
		*/
		public function getAllRecordsFromTable ($table) {
			$this->_selectSql = 'SELECT * FROM '.$table;
			return $this->_selectSql;
		}

		/**
		* To return all columns using SQL SELECT Query with conditions*
		* @access public
		* @param  string $table Array $conditionalData
		* @return Array
		*/
		public function getAllRecordsBasedOnAndCondition ($table,$conditionalData) {
			$whereClauses = [];
			foreach ($conditionalData as $key => $value) {
				$clause= $key." = '".$value."'";
				array_push($whereClauses, $clause);
			}
			if (sizeof($whereClauses)==1) {
				$this->_selectSql = 'SELECT * FROM '.$table.' WHERE '.$whereClauses[0];
			}
			else {
				$this->_selectSql = 'SELECT * FROM '.$table.' WHERE '.implode(' AND ', $whereClauses);
			}

			return $this->getResultFromSelectSql($this->_selectSql);
		}


		/**
		* To execute a SQL Query on Database.*
		* @access public
		* @param  string $sql
		* @return boolean
		*/
		public function executeSql ($sql) {
			$executionStatus = FALSE;
			if (!mysqli_query($this->_connection, $sql)) {
				 writeMessageToLogFile("Prepare failed: (" . $this->_connection->errno . ") " . $this->_connection->error);
			}
			else {
				$executionStatus = TRUE;
			}
			return $executionStatus;
		}
		
		/**
		* To get result of a SQL SELECT Query*
		* @access public
		* @param  string $sql
		* @return Resource
		*/
		public function getResultFromSelectSql ($sql) {
            $result = $this->_connection->query($sql);
            if (!$result) {
                writeMessageToLogFile($this->getErrorInStatementExecution());
            }
			return $result;
		}

		/**
		* To get result of a SQL UPDATE Query*
		* @access public
		* @param  string $sql
		* @return Resource
		*/
		public function getUpdateSqlResult ($updateSql) {
            $result = $this->_connection->query($updateSql);
            if (!$result) {
                writeMessageToLogFile($this->getErrorInStatementExecution());
            }
            return $result;
		}

		/**
		* To get result of a SQL DELETE Query*
		* @access public
		* @param  string $sql
		* @return Resource
		*/
		public function getDeleteSqlResult ($deleteSql) {
            $result = $this->_connection->query($deleteSql);
            if (!$result) {
                writeMessageToLogFile($this->getErrorInStatementExecution());
            }
            return $result;
		}

		/**
		* To get last inserted row's id*
		* @access public
		* @param  none
		* @return string
		*/
		public function getLastDataInsertId () {
            $result = $this->_connection->insert_id;
            if (!$result) {
                writeMessageToLogFile($this->getErrorInStatementExecution());
            }
			return $result;
		}

		/**
		 * To begin a DB Transaction*
		 * @access public
		 * @param  None
		 * @return None
		 */
		public function beginTransaction () {
			if (!$this->_connection->begin_transaction() ){
				writeMessageToLogFile("could not start transaction..");
			}
		}

		/**
		 * To commit a DB Transaction*
		 * @access public
		 * @param  None
		 * @return None
		 */
		public function commitTransaction () {
			if (!$this->_connection->commit()) {
				writeMessageToLogFile("Couldn't commit a DB Transaction.");
				$this->rollbackTransaction();
			}
		}

		/**
		 * To Rollback a DB Transaction*
		 * @access public
		 * @param  None
		 * @return None
		 */
		public function rollbackTransaction () {
			if (!$this->_connection->rollback()) {
				writeMessageToLogFile("Couldn't rollback a DB Transaction.");
			}
		}

		/**
		* To terminate a database connection*
		* @access public
		* @param  None
		* @return None
		*/
		public function closeConnection () {
			mysqli_close($this->_connection);
		}

		/**
		* To insert data into a table*
		* @access public
		* @param  string $tableName Array $data
		* @return boolean
		*/
		public function insertDataIntoTable ($tableName,$data) {
			$insertionStatus = FALSE;
			if ($this->getInsertSqlForData($tableName,$data) && $this->_insertSql && $this->executeSql($this->_insertSql)) {
					$insertionStatus = TRUE;	
			}
			return $insertionStatus;
		}

		
		/**
		* To get SQL Update Query using data and conditions.*
		* @access public
		* @param  Array $data Array $conditions string $tableName
		* @return string
		*/
		public function getUpdateSqlForDataWithCondition ($data,$conditions,$tableName) {
			$updatedata = [];
			foreach ($data as $key => $value) {
				array_push($updatedata, $key." = '".$value."' ");
			}
			$conditionData = [];
			foreach ($conditions as $key => $value) {
				array_push($conditionData, $key." = '".$value."' ");
			}
			$this->_updateSql = "UPDATE ".$tableName." SET ".implode(",", $updatedata);
			if (sizeof($conditionData)>1) {
				$this->_updateSql .= " WHERE ".implode(" AND ", $conditionData);
			}
			else if (sizeof($conditionData)==1) {
				$this->_updateSql .= " WHERE ".$conditionData[0];
			}
			return $this->_updateSql;
		}

		/**
		 * To Get/Create the id for an Employee's Skill.*
		 * @access public
		 * @param  string $skill
		 * @return int
		 */
		public function getIdForEmployeeSkill ($skill) {
			$skill = strtolower(trim($skill));
			$skillID = "";
			$attr = ['id'];
			$condition['name'] = $skill;
			$tableName = "skill_info";
			$result = $this->getRecordsAttributesOnCondition($attr,$condition,$tableName);
			if ($result->num_rows > 0) {
				while ($row = $result->fetch_assoc()) {
					$skillID = $row['id'];
				}
			}
			else {
				$data['name'] = $skill;
				if ($this->executeSql($this->getInsertSqlForData($tableName,$data))) {
					$skillID = $this->getLastDataInsertId();
				}
			}
			return $skillID;
		}

		/**
		 * To Get/Create the id for an HR.*
		 * @access public
		 * @param  string $hrName
		 * @return int
		 */
		public function getIdForHrName ($hrName) {
			$hrName = strtolower(trim($hrName));
			$hrID = "";
			$attr  = ['id'];
			$condition['name'] = $hrName;
			$tableName = "hr_info";
			$result = $this->getRecordsAttributesOnCondition($attr,$condition,$tableName);
			if ($result->num_rows > 0) {
				while ($row = $result->fetch_assoc()) {
					$hrID = $row['id'];
				}
			}
			else {
				$data['name'] = $hrName;
				if ($this->executeSql($this->getInsertSqlForData($tableName,$data))) {
					$hrID = $this->getLastDataInsertId();
				}
			}
			return $hrID;
		}

		/**
		 * To insert all employees in the Database.*
		 * @access public
		 * @param  Array $employeeArray
		 * @return Array
		 */
		public function insertEmployeesIntoDB ($employeeArray) {
			$insertionStatus = false;
			if (isset($employeeArray['valid']) && isset($employeeArray['emplist']) && is_array($employeeArray['emplist'])) {
				$employeeInsertion['status'] = true;
				$empObjects = $employeeArray['emplist'];
				foreach ($empObjects as $empId => $employeeObject) {
					$this->beginTransaction();
					$empId = mysqli_real_escape_string($this->_connection,$empId);
					$basicInfo = $employeeObject['basic_info'];
					$data = [];
					$data['id'] = $empId;
					$data['first'] = mysqli_real_escape_string($this->_connection,$basicInfo['first']);
					$data['last'] = mysqli_real_escape_string($this->_connection,$basicInfo['last']);
					$data['created_by'] = mysqli_real_escape_string($this->_connection,$basicInfo['created_by']);
					$data['updated_by'] = mysqli_real_escape_string($this->_connection,$basicInfo['updated_by']);
					$tableName = "employee_mains";
					if ($this->executeSql($this->getInsertSqlForData($tableName,$data))) {
						$skills = $employeeObject['skills'];
						$skillInsertion['status'] = true;
						if (is_array($skills) && !empty($skills)) {
							foreach ($skills as $key => $skill) {
								$data = [];
								$data['employee_id'] = $empId;
								$data['skill'] = $skill;
								$tableName = "employee_skills";
								if (!$this->executeSql($this->getInsertSqlForData($tableName,$data))) {
									$skillInsertion['status'] = false;
									$employeeInsertion['status'] = false;
									break;
								}
							}
						}
						$stackInfo = $employeeObject['stack_info'];
						$data = [];
						$data['employee_id'] = $empId;
						$data['stack_id'] = mysqli_real_escape_string($this->_connection,$stackInfo['stack_id']);
						$data['stack_nick_name'] = mysqli_real_escape_string($this->_connection,$stackInfo['stack_nick_name']);
						$tableName = "employee_stack_info";
						if (!$skillInsertion['status'] || !$this->executeSql($this->getInsertSqlForData($tableName,$data))) {
							$this->rollbackTransaction();
							$employeeInsertion['status'] = false;
							break;
						}
						else {
							$this->commitTransaction();
						}
					}
				}
				$insertionStatus = $employeeInsertion['status'];
			}
			return $insertionStatus;
		}

		/**
		 * To retrieve all employees from the Database.*
		 * @access public
		 * @param  None
		 * @return Array
		 */
		public function getAllEmployeeDataFromDB () {
			$employees = [];
			$this->_selectSql = "SELECT em.id, em.first, em.last,GROUP_CONCAT(UPPER(skilli.name)) AS 'skills', esi.stack_id, esi.stack_nick_name, UPPER(hri1.name) AS 'created_by', UPPER(hri2.name) AS 'updated_by'
								FROM employee_mains em
								JOIN employee_stack_info esi ON ( em.id = esi.employee_id ) 
								JOIN employee_skills es ON ( es.employee_id = em.id )
								JOIN skill_info skilli ON ( skilli.id = es.skill )
								JOIN hr_info hri1 ON ( em.created_by = hri1.id ) 
								JOIN hr_info hri2 ON ( em.updated_by = hri2.id )
								GROUP BY em.id;";
			$result = $this->getResultFromSelectSql($this->_selectSql);
			if ($result->num_rows > 0) {
				while ($row = $result->fetch_assoc()) {
					$employee = [];
					$employee['EmpID'] = htmlspecialchars($row['id']);
					$employee['First'] = htmlspecialchars($row['first']);
					$employee['Last'] = htmlspecialchars($row['last']);
					$employee['skills'] = explode(",",htmlspecialchars($row['skills']));
					$employee['StackId'] = htmlspecialchars($row['stack_id']);
					$employee['StackNickname'] = htmlspecialchars($row['stack_nick_name']);
					$employee['CreatedBy'] = htmlspecialchars($row['created_by']);
					$employee['UpdatedBy'] = htmlspecialchars($row['updated_by']);
					array_push($employees,$employee);
				}
			}
			return $employees;
		}
	}



?>
