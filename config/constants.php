<?php

	/*** constants.php for defining all the constants related to Project*
	* PHP version 5*
	* @category None
	* @package None
	* @subpackage None
	* @author Ashish Kumar
	* @license Project Name None
	* @link None
	*/
	
	// Database configuration
	define('HOST', 'localhost');

	// Path to save Image 
	define('USERNAME', 'root');

	// Password of user
	define('PASSWORD', 'mindfire');

	// Database name
	define('DB', 'DB_Assignment');

    // Log File Path....
    define('LOGDIR','logs');

	// File Upload Directory..
	define('UPLOADDIR','resources');

	// Employees Data Directory..
	define('EMPDATADIR','resources');

	// Delimiter used in CSV File to seperate Fields.
	define('DELIMITER',";");
?>
