<?php
    /*** allemployees.php for listing employees on UI.
     *
     * PHP version 5
     * @category None
     * @package None
     * @subpackage None
     * @author Ashish Kumar
     * @license Project Name None
     * @link None
     */
    require_once('config/constants.php');
    require_once('libraries/Database.php');
    $dbObject = new Database();
    $employees = $dbObject->getAllEmployeeDataFromDB();
    $dbObject->closeConnection();
    $maxSkills = 0;
    foreach ($employees as $key => $employee) {
        $skills = $employee['skills'];
        if (count($skills) > $maxSkills) {
            $maxSkills = count($skills);
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            All Employees
        </title>
        <!--Import Google Icon Font-->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css"  media="screen,projection"/>

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
        <script type="text/javascript" src="js/allemployees.js"></script>
    </head>
    <body>
        <div class="container">
            <br><br><br><br>
            <div class="row card-panel">
                <div class="col offset-m3 m6">
                    <div class="progress">
                        <div class="determinate" style="width: 0%"></div>
                    </div>
                </div>
                <div class="m3">
                    <strong id="msg"></strong>
                </div>
                <form id = "fileUploadForm" enctype="multipart/form-data" method="post">
                    <div class="col offset-m2 m7">
                        <div class="file-field input-field">
                            <div class="btn">
                                <span>Browse</span>
                                <input type="file" name="empDataFile" id="empDataFile">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text" placeholder="Upload Employee Data File">
                            </div>
                        </div>
                    </div>
                    <div class="col m3">
                        <button type="submit" class="waves-effect pink accent-2 btn">
                            <i class="material-icons right">navigation</i>Upload
                        </button>
                    </div>
                </form>
                <div class="col offset-m3 m6" id="errors">
                    <h6>Following Problems found with the Current File :</h6>
                    <div style="color: red;">

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col offset-m2 m8">
                    <h2>
                        Imported CSV Data
                    </h2>
                </div>
            </div>
            <div class="card-panel light-blue lighten-2 z-depth-3">
                <div class="row">
                    <table id="users" class="highlight responsive-table">
                        <thead>
                        <tr>
                            <th>EmpID</th>
                            <th>First</th>
                            <th>Last</th>
                            <?php
                            for ($i=1 ;$i<=$maxSkills;$i++) {
                                $skillColumn = "<th>"."Skill".$i."</th>";
                                echo $skillColumn;
                            }
                            ?>
                            <th>Stack ID</th>
                            <th>Stack Nickname</th>
                            <th>Created By</th>
                            <th>Updated By</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $row = "";
                            foreach ($employees as $key => $emp) {
                                $empSkills = count($emp['skills']);
                                $row .= "<tr><td>".$emp['EmpID']."</td>
                                              <td>".$emp['First']."</td>
                                              <td>".$emp['Last']."</td>";
                                for ($i=1;$i<=$maxSkills;$i++) {
                                    $skill = "";
                                    if ($i <= $empSkills) {
                                        $skill = $emp['skills'][($i-1)];
                                    }
                                    $row .= "<td>".$skill."</td>";
                                }
                                $row .= "<td>".$emp['StackId']."</td>
                                 <td>".$emp['StackNickname']."</td>
                                 <td>".$emp['CreatedBy']."</td>
                                 <td>".$emp['UpdatedBy'].
                                    "</td></tr>";
                            }
                            echo $row;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <br><br>
    </body>
</html>
