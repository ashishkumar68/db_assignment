<?php

    /*** ValidationHelper.php for validating input Data for DB.
     *
     * PHP version 5
     * @category Helper
     * @package helpers
     * @subpackage None
     * @author Ashish Kumar
     * @license Project Name None
     * @link None
     */
    require_once('libraries/Database.php');

    /**
     * To Validate File Input Data. *
     * @access public
     * @param  Array $attrs, Array $records, Object $dbObject
     * @return Array
     */
    function validateFileRecords ($attrs,$records,$dbObject) {
        $errorlist['status'] = true;
        $skills = [];
        $errors = [];
        $columnErrors = [];
        foreach ($attrs as $key => $attr) {
            if (strpos(strtolower($attr),"skill") !== false) {
                array_push($skills,$attr);
            }
        }
        $columnsValidationChecks = [
            'REQUIRED' => ['EmpID','First','Last','CreatedBy','UpdatedBy'],
            'DEPENDENCY' => ['StackID','StackNickname'],
            'COLUMNDUPLICACY' => $attrs
        ];
        foreach ($columnsValidationChecks as $key => $check) {
            switch($key) {
                case 'REQUIRED' :
                    foreach ($check as $key1 => $attr) {
                        if (!in_array($attr,$attrs)) {
                            array_push($columnErrors,"$attr Column Missing.");
                        }
                    }
                    break;
                case 'DEPENDENCY' :
                    if ((!in_array($check[0],$attrs) && in_array($check[1],$attrs)) || (!in_array($check[1],$attrs) && in_array($check[0],$attrs))) {
                        array_push($columnErrors,"Any one of column $check[0] or $check[1] is missing.");
                    }
                    break;
                case 'COLUMNDUPLICACY' :
                    foreach ($check as $key => $attr) {
                        foreach ($check as $key1 => $attr1) {
                            if (($key != $key1) && ($attr == $attr1)) {
                                array_push($columnErrors,"Remove Duplicate Columns.");
                            }
                        }
                    }
                    break;
            }
        }
        $validationChecks = [
            'REQUIRED' =>  ['EmpID','First','Last','CreatedBy','UpdatedBy'],
            'DEPENDENCY' => ['StackID','StackNickname'],
            'REDUNDANCY' => ['EmpID','StackID'],
            'SKILLVALIDATE' => $skills
        ];

        foreach ($records as $key => $record) {
            foreach ($validationChecks as $key1 => $check) {
                switch ($key1) {
                    case 'REQUIRED' :
                        foreach ($check as $key2 => $attr) {
                            if (!isset($record[$attr]) ||!$record[$attr] || $record[$attr] == "") {
                                $errors = addErrorMessageToArray($errors,1+$key,$attr." Field Required");
                            }
                        }
                        break;
                    case 'DEPENDENCY' :
                        if (($record[$check[0]] || $record[$check[1]]) && ($record[$check[0]] == "" || $record[$check[1]] == "")) {
                            $errors = addErrorMessageToArray($errors,1+$key,"Please provide both entries $check[0] and $check[1]");
                        }
                        break;
                    case 'REDUNDANCY' :
                        if (!validateInputEmployeeID($dbObject,$record[$check[0]])) {
                            $errors = addErrorMessageToArray($errors,1+$key,"Duplicate Entry of EmpID");
                        }
                        if (!validateInputStackID($dbObject,$record[$check[1]])) {
                            $errors = addErrorMessageToArray($errors,1+$key,"Duplicate Entry of StackID");
                        }
                        break;
                }
            }
            $skills = [];
            foreach ($validationChecks['SKILLVALIDATE'] as $key1 => $skill) {
                array_push($skills,$record[$skill]);
            }
            if (!validateInputSkills($skills)) {
                $errors = addErrorMessageToArray($errors,1+$key,"Repeated Skills ");
            }
            if (sizeof($attrs) > sizeof($record)) {
                array_push($columnErrors,"Invalid Number of Attributes in record ".(1+$key));
            }
        }
        $errorlist['errors'] = $errors;
        $errorlist['columnErrors'] = $columnErrors;
        if (sizeof($errors) > 1 ) {
            $errorlist['status'] = false;
        }
        return $errorlist;
    }

    /**
     * To Write Error Message in Array. *
     * @access public
     * @param  Array $errorArray, int $key, string $msg
     * @return Array
     */
    function addErrorMessageToArray ($errorArray,$key,$msg) {

        if (isset($errorArray[$key])) {
            $errorArray[$key] .= ",".$msg;
        }
        else {
            $errorArray[$key] = $msg." in record ".$key;
        }
        return $errorArray;
    }

    /**
     * To Validate EmpID of Record. *
     * @access public
     * @param  Object $dbObject, string $empID
     * @return boolean
     */
    function validateInputEmployeeID ($dbObject,$empID) {
        $validationStatus = true;
        $attr = ['first'];
        $condition['id'] = $empID;
        $tableName = "employee_mains";
        $result = $dbObject->getRecordsAttributesOnCondition($attr,$condition,$tableName);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                if ($row['first'] && $row['first'] != "") {
                    $validationStatus = false;
                    break;
                }
            }
        }
        return $validationStatus;
    }

    /**
     * To Validate StackID of Input Employee. *
     * @access public
     * @param  Object $dbObject, string $StackID
     * @return boolean
     */
    function validateInputStackID ($dbObject,$StackID) {
        $validationStatus = true;
        $attr = ['stack_nick_name'];
        $condition['stack_id'] = $StackID;
        $tableName = "employee_stack_info";
        $result = $dbObject->getRecordsAttributesOnCondition($attr,$condition,$tableName);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                if ($row['stack_nick_name'] && $row['stack_nick_name'] != "") {
                    $validationStatus = false;
                    break;
                }
            }
        }
        return $validationStatus;
    }

    /**
     * To Validate input skills. *
     * @access public
     * @param  Array $skills
     * @return boolean
     */
    function validateInputSkills ($skills) {
        $validationStatus = true;
        foreach ($skills as $key => $skill) {
            foreach ($skills as $key1 => $otherskill) {
                if (($key != $key1) && ($skill != "" && $otherskill !="") && (strtolower($skill) == strtolower($otherskill))) {
                    $validationStatus = false;
                }
            }
        }
        return $validationStatus;
    }
?>