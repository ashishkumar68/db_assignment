<?php

    /*** UtilityHelper for all the Utilities required in Project
     *
     * PHP version 5
     * @category Helper
     * @package helpers
     * @subpackage None
     * @author Ashish Kumar
     * @license Project Name None
     * @link None
     */

    require_once('config/constants.php');
    require_once('libraries/Database.php');
    require_once('helpers/ValidationHelper.php');
    /**
     * To get all the lines from csv file.
     * @access public
     * @param  string $fileName
     * @return Array
     */
    function fetchAllRecordsFromFile ($fileName) {

        $file['readstatus'] = false;
        try {
            if (($handle = fopen($fileName, "r")) !== FALSE) {
                $records = [];
                while (($data = fgetcsv($handle,1000,DELIMITER)) !== FALSE) {
                    array_push($records,$data);
                }
                $file['readstatus'] = true;
                $file['records'] = $records;
                fclose($handle);
            }
            else {
                writeMessageToLogFile("Error in Opening Employees Data File..");
            }
        }
        catch (Exception $e) {
            writeMessageToLogFile($e->getMessage());
        }
        return $file;
    }

    /**
     * To convert file data lines into employees array.
     * @access public
     * @param  Array $records
     * @return Array
     */
    function convertFileDataIntoEmployeeArray ($file) {
        $employees['convertstatus'] = false;
        if ($file['readstatus']) {
            $records = $file['records'];
            foreach ($records[0] as $key => $attr) {
                $records[0][$key] = trim($attr);
            }
            $employees['attributes'] = $records[0];
            $empArray = [];
            foreach ($records as $key => $record) {
                if ($key > 0) {
                    $empAttrs = $record;
                    $emp = [];
                    $attributes = $employees['attributes'];
                    foreach ($attributes as $key => $attr) {
                        $emp[$attr] = trim($empAttrs[$key]);
                    }
                    array_push($empArray,$emp);
                }
            }
            $employees['convertstatus'] = true;
            $employees['empArray'] = $empArray;
        }
        return $employees;
    }

    /**
     * To convert records employees Objects Array.
     * @access public
     * @param  Array $employees
     * @return Array
     */
    function getEmployeeArrayFromRecords ($employees,$dbObject) {
        $employeeArray['valid'] = false;
        if ($employees['convertstatus']) {
            $empArray = [];
            foreach ($employees['empArray'] as $key => $emp) {
                $employee = [];
                $basicInfo['first'] = $emp['First'];
                $basicInfo['last'] = $emp['Last'];
                $basicInfo['created_by'] = $dbObject->getIdForHrName($emp['CreatedBy']);
                $basicInfo['updated_by'] = $dbObject->getIdForHrName($emp['UpdatedBy']);
                $skills = [];
                foreach ($emp as $attr => $value) {
                    if (strpos($attr,"Skill") !== false) {
                        array_push($skills,$attr);
                    }
                }
                $empSkills = [];
                foreach ($skills as $key1 => $skill) {
                    if (isset($emp[$skill]) && trim($emp[$skill]) != "") {
                        array_push($empSkills,$dbObject->getIdForEmployeeSkill($emp[$skill]));
                    }
                }
                $stackInfo = [];
                if (isset($emp['StackID']) && $emp['StackID'] != "") {
                    $stackInfo['stack_id'] = $emp['StackID'];
                    $stackInfo['stack_nick_name'] = $emp['StackNickname'];
                }
                $employee['basic_info'] = $basicInfo;
                $employee['skills'] = $empSkills;
                $employee['stack_info'] = $stackInfo;
                $empArray[$emp['EmpID']] = $employee;
                $employeeArray['valid'] = true;
                $employeeArray['emplist'] = $empArray;
            }
        }
        return $employeeArray;
    }

    /**
     * To Write Message in Errors log file. *
     * @access public
     * @param  string $message
     * @return None
     */
    function writeMessageToLogFile ($message) {
        $date = date("Y-m-d", time());
        $msgTimestamp = date("Y-m-d H:i:s", time());
        $fileName = LOGDIR."/"."log_".$date.".log";

        // Check if dir is not present then create directory.
        if (!is_dir(LOGDIR)) {
            exec("chmod 777 .");
            mkdir(LOGDIR,"0777");
        }
        // Changing permissions on Directory if Write Permissions are not given..
        if (!is_writable(LOGDIR)) {
            exec("chmod 777 ".LOGDIR);
        }

        // Check if file is already present..
        if (file_exists($fileName)) {
            // Then open file in append mode.
            $fd = fopen($fileName, "a");
        }
        else {
            // else open file in write mode.
            exec("chmod 777 ".LOGDIR."*");
            $fd = fopen($fileName, "w");
        }
        if ($fd) {
            // Now if file is open then write this message in the file.
            fwrite($fd, $msgTimestamp." -> ".$message."\n");
            fclose($fd);
        }

    }
    /*$dbObject = new Database();
    $tableRecords = convertFileDataIntoEmployeeArray(fetchAllRecordsFromFile("../".EMPDATADIR."/Test - Parse Sheet.csv"),$dbObject);
    $attributes = $tableRecords['attributes'];
    $records = $tableRecords['empArray'];
    print_r(validateFileRecords($attributes,$records,$dbObject));
    $dbObject->closeConnection();*/
?>
